/*
A parser for the input of the programm

Pietro Marino
*/

#include "cparser.h"


//_______________________________________________________________
CParser::CParser(int argc_, char* argv_[])
{
    /* constructor */
    fargc = argc_;
    fargv.resize(fargc);
    /* copy argv_ in fargv */
    std::copy(argv_, argv_+fargc, fargv.begin());
    
    std::vector<std::string>::iterator it1, it2;
    it1 = fargv.begin();
    it2 = it1 + 1;
    
    while (true)
    {
        if (it1 == fargv.end()) break;
        if (it2 == fargv.end()) break;

        if ((*it1)[0] == '-')
            fdecod[*it1] = *(it2);

        /* for options without argument: "--option" */
        if ((*it1)[1] == '-')
            fdecod[*it1] = *(it1);
        if ((*it2)[1] == '-')
            fdecod[*it2] = *(it2);
        /* END no argument options */

        it1++;
        it2++;
    };
}


//_______________________________________________________________
std::string CParser::GetArg(std::string s)
{
    /* return the mapped argument */
    if (fdecod.find(s) != fdecod.end())
        return fdecod[s];
    else
        return "";
}
