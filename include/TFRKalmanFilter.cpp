/*!
 *  @author    Simon Stemmle
 *  @brief     Kalman filter to fit a TFRtrack
 */

#include "TFRKalmanFilter.h"

using namespace std;

//----------
// Fit a TFRTrack using a Kalman filter
//----------
void TFRKalmanFilter::FitTrack(TFRTrack *track){

  //Create the fit nodes of the track using the hits (clusters)
  track->CreateFitnodes();

  //Now we access these nodes
  TFRFitnodes *nodes = track->GetFitnodes();
  
  /////////////////////////////////
  //Run the Kalman filter forwards
  /////////////////////////////////
  
  //Create the seed state at the first node
  CreateSeedState(nodes, track);
  
  //Update this state with the first measurement
  UpdateState((TFRFitnode*)nodes->At(0), true);
  
  //Loop over all nodes
  for(int i=1; i<nodes->GetEntries(); i++){

    //Predict the state at the next node
    PredictState((TFRFitnode*)nodes->At(i-1), (TFRFitnode*)nodes->At(i));
    
    //Update this state with the respective measurement
    UpdateState((TFRFitnode*)nodes->At(i), true);
    
  }  //loop over the nodes
  
  //////////////////////////////////
  //Run the Kalman filter backwards
  //////////////////////////////////
  
  //Create the seed state at last node (using information of the forward filtering)
  CreateBackwardSeedState((TFRFitnode*)nodes->Last());
  
  //Update this state with the last measurement
  UpdateState((TFRFitnode*)nodes->Last(), false);
  
  //loop over all nodes
  for(int i=nodes->GetEntries()-2; i>=0; i--){

    //Predict the state at the next node
    PredictState((TFRFitnode*)nodes->At(i+1), (TFRFitnode*)nodes->At(i));
    
    //Update this state with the respective measurement
    UpdateState((TFRFitnode*)nodes->At(i), false);

  }  //loop over the nodes
  
  //////////////////////////////////////////
  //Run the smoother and calculate the chi2
  //////////////////////////////////////////

  //chi2 and degrees of freedom
  double chi2 = 0;
  int NdoF = 0;

  //loop over all nodes and smoothe (average forward and backward)
  for(int i=0; i<nodes->GetEntries(); i++){
   SmoothState((TFRFitnode*)nodes->At(i));
   
   //Add the smoothed state to the state list of the track
   track->AddState(&( ((TFRFitnode*)nodes->At(i))->StateSmoothed() ));
   
   //Add the chi2 of this node to the global chi2
   chi2+=((TFRFitnode*)nodes->At(i))->StateUpdated(true).GetChi2();
   
   //Add the dimension of the measurement to NdoF
   NdoF+=((TFRFitnode*)nodes->At(i))->GetCluster().GetMeasure().GetNrows();
  }  //loop over the nodes
  
  //NdoF:
  //If there is B field in the detector: 5 parameters
  //If there is no B field in the detector: 4 parameters
  if(m_propagator->GetGeometry()->GetBFieldStartZ()>= ((TFRFitnode*)nodes->Last())->GetCluster().GetZ() 
    || m_propagator->GetGeometry()->GetBMag()==0){
    NdoF-=4;
  }
  else{
    NdoF-=5;
  }

  //Set chi2 related quantities 
  track->SetChi2(chi2, true);
  track->SetChi2NdoF(chi2 / (NdoF), true);
  track->SetNdoF(NdoF, true);

  
  
  ////////////////////////////////
  //Add a state at z =0 (~vertex)
  ////////////////////////////////
  
  //Get the smoothed state closest to z=0
  TFRState *state = new TFRState(); 
  *state = ((TFRFitnode*)nodes->At(0))->StateSmoothed();
  
  //Extrapolate this state to z=0
  m_propagator->PropagateState(state, 0);
  
  //Add the extrapolated state to the track
  track->AddState(state);
  
}

//-------------
// Create a seed state at the first node (Works only if the first two clusters are from pixel sensors (x,y))
//-------------
void TFRKalmanFilter::CreateSeedState(TFRFitnodes *nodes, TFRTrack *track){
  
  //Array to be filled with the state variables (x,y,tx,ty,q/p)
  double x[5];
  
  //Get the first two fit nodes
  TFRFitnode *node0=(TFRFitnode*)nodes->At(0);
  TFRFitnode *node1=(TFRFitnode*)nodes->At(1);
  
  //Determine x,y position from the first hit
  x[0] = node0->GetCluster().GetMeasure()[0];
  x[1] = node0->GetCluster().GetMeasure()[1];
  
  //Determine tx,ty from the first two hits
  x[2] = node1->GetCluster().GetMeasure()[0]
         -node0->GetCluster().GetMeasure()[0];
  x[3] = node1->GetCluster().GetMeasure()[1]
         -node0->GetCluster().GetMeasure()[1];
  x[2] /= node1->GetCluster().GetZ()
         -node0->GetCluster().GetZ(); 
  x[3] /= node1->GetCluster().GetZ()
         -node0->GetCluster().GetZ();
  
  //Get an estimate for qop from the track (e.g. from a previous chi2 fit)
  x[4] = track->GetCharge()/track->GetMomentumMod();
  
  //Set the covariance of this state
  double P[25]={0};

  //Set large uncertainties (it is crucial that no information is used twice)
  P[0*5+0] = 100*100;
  P[1*5+1] = 100*100;
  P[2*5+2] = 1*1;
  P[3*5+3] = 1*1;
  P[4*5+4] = 0.001*0.001;
  
  //Set the forward predicted state of the first node
  node0->StatePredicted(true).SetStateVect(x);
  node0->StatePredicted(true).SetP(P);
  node0->StatePredicted(true).SetZ(node0->GetCluster().GetZ());
}

//-----------
// Create Backwards seed state from forward updated state
//-----------
void TFRKalmanFilter::CreateBackwardSeedState(TFRFitnode *node){

  //Get the forward updated state of this node
  TFRState state = node->StateUpdated(true);
  
  //Set large uncertainties (it is crucial that no information is used twice)
  double P[25]={0};
  
  //Set large uncertainties
  P[0*5+0] = 100*100;
  P[1*5+1] = 100*100;
  P[2*5+2] = 1*1;
  P[3*5+3] = 1*1;
  P[4*5+4] = 0.001*0.001;
  
  //Set the backward predicted state of this node
  state.SetP(P);
  node->StatePredicted(false) = state;
}

//-------------
// Update the state with the measurement information
//-------------
void TFRKalmanFilter::UpdateState(TFRFitnode *node, bool forward){
  
  //Get what we need from the node
  //The measurement vector
  TVectorD meas  = node->GetCluster().GetMeasure();

  //The measurement covariance
  TMatrixD R  = node->GetCluster().GetCovariance();

  //The predicted statevector
  TVectorD s  = node->StatePredicted(forward).GetStateVect();

  //The predicted covariance
  TMatrixD P  = node->StatePredicted(forward).GetP();
  
  //The projection matrix (projecting a state to the measurement space)
  TMatrixD H  = node->GetProjectionMatrix();
  
  //Do the actual Kalman Formalism
  
  //Measurement residual
  TVectorD res =  meas - H*s;
  
  //Innovation covariance S
  TMatrixD HT(TMatrixD::kTransposed,H);
  TMatrixD S =  H*P*HT + R;
  
  //Kalman gain K
  TMatrixD Sinv(TMatrixD::kInverted,S);
  TMatrixD K = P*HT*Sinv;
  
  //New updated statevector and covariance
  TVectorD s_up = s + K*res;
  TMatrixD P_up = P - (K*H)*P;
  
  //Set the updated state of this node
  node->StateUpdated(forward) = TFRState(s_up,
					 node->StatePredicted(forward).GetZ(),
					 0, P_up);
  
  //Determine chi2 of this updated state with the measurement
  
  //Residual
  TVectorD res_up = meas - H*s_up;
  
  //Covariance of the residual
  TMatrixD res_up_cov = R-H*P_up*HT;
  TMatrixD res_up_cov_inv(TMatrixD::kInverted,res_up_cov);
  
  //chi2 = rT*R(-1)*r
  double chi2 = res_up*(res_up_cov_inv*res_up);
  node->StateUpdated(forward).SetChi2(chi2);
  
}

//-------------
// Predict the state at node2 given the state at node1
//-------------
void TFRKalmanFilter::PredictState(TFRFitnode *node1, TFRFitnode *node2){

  //Determine wether we are running forwards or backwards
  bool forward = node1->GetCluster().GetZ() < node2->GetCluster().GetZ() ? true : false;
  
  //Get the previous updated state
  TFRState state = node1->StateUpdated(forward);

  //Extrapolate this state to the hit of node 2
  m_propagator->PropagateState(&state, node2->GetCluster().GetZ());

  //Set the predicted state of node 2 to be this state
  node2->StatePredicted(forward)= state;
}

//-------------
// Smooth the states by averaging forward and backward filtering 
//-------------
void TFRKalmanFilter::SmoothState(TFRFitnode *node){

  //Get what we need from the node
  //Average the backward updated and forward predicted state
  //The other way around would be also fine,
  //but we have to take care that we don't use infomration twice
  
  //The measurement vector
  TVectorD meas  = node->GetCluster().GetMeasure();//TODO take care of correct GetMeasure

  //The measurement covariance
  TMatrixD R  = node->GetCluster().GetCovariance();//TODO take care of correct GetCovariance

  //The backward updated statevector
  TVectorD s_backw = node->StateUpdated(false).GetStateVect();

  //The backwards updated covariance
  TMatrixD P_backw = node->StateUpdated(false).GetP();
  
  //The predicted statevector
  TVectorD s_forw  = node->StatePredicted(true).GetStateVect();
  
  //The predicted covariance
  TMatrixD P_forw  = node->StatePredicted(true).GetP();

  //The projection matrix
  TMatrixD H     = node->GetProjectionMatrix();
  TMatrixD HT(TMatrixD::kTransposed,H);
  
  //Do the Kalman Formalism (see update method) with H=1
  
  //Measurement residual
  TVectorD res = s_forw - s_backw;

  //Innovation covariance S
  TMatrixD S = P_backw + P_forw;
  TMatrixD Sinv(TMatrixD::kInverted,S);

  //Kalman gain K
  TMatrixD K = P_backw*Sinv;
  
  //New smoothed statevector and covariance
  TVectorD s_sm = s_backw + K*res;
  TMatrixD P_sm = K*P_forw;
  
  //Set the smoothed State
  node->StateSmoothed() = TFRState(s_sm,
				   node->StateUpdated(true).GetZ(),
				   0, P_sm);
  
  //Calculate the chi2 of this smoothed state to the measurement
  
  //Residual
  TVectorD res_sm = meas - H*s_sm;

  //Covariance of residual
  TMatrixD res_sm_cov = R - H*P_sm*HT;
  TMatrixD res_sm_cov_inv(TMatrixD::kInverted, res_sm_cov);
  
  //There seems to be no method for a scalar product of TVectorD's???
  //chi2 = rT*R(-1)*r
  double chi2 = res_sm * (res_sm_cov_inv * res_sm);
  node->StateSmoothed().SetChi2(chi2);
}

