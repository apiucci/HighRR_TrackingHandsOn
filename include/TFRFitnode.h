#ifndef INCLUDE_TFRFITNODE_H
#define INCLUDE_TFRFITNODE_H 1

/*!
 *  @author    Simon Stemmle
 *  @brief     A fitnode contains a hit(cluster), the respective state (forward and backward predicted and updated and smoothed) and related quantities
 */

//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>

//ROOT libraries
#include "TROOT.h"

//custom libraries
#include "TFRState.h"
#include "TFRCluster.h"

using namespace std;

class TFRFitnode : public TObject {
 public:

  /*! Empty constructor */
  TFRFitnode(){ };

  /*! Standard constructor */
  TFRFitnode(TFRCluster cluster) :
    m_cluster(cluster) {};

  /*! Destructor */
  virtual ~TFRFitnode( ){ };

  /*! Get the TFRCluster */
  TFRCluster GetCluster() {return m_cluster;};
  
  /*! Get the respective TFRState */
  TFRState& StatePredicted(bool forward) {return forward ? m_state_f_p : m_state_b_p;};
  
  /*! Get the respective TFRState */
  TFRState& StateUpdated(bool forward) {return forward ? m_state_f_u : m_state_b_u;};
  
  /*! Get the respective TFRState */
  TFRState& StateSmoothed() {return m_state_sm;};
  
  /*! Get the projection matrix */
  TMatrixD GetProjectionMatrix(){return m_cluster.GetLayer().GetProjectionMatrix();};
  
 protected:

 private:

  /*! Cluster of this node */
  TFRCluster m_cluster;

  /*! State of the track at this node (forward predicted)*/ 
  TFRState m_state_f_p;

  /*! State of the track at this node (forward updated)*/ 
  TFRState m_state_f_u;

  /*! State of the track at this node (backward predicted)*/ 
  TFRState m_state_b_p;

  /*! State of the track at this node (backward updated)*/ 
  TFRState m_state_b_u;

  /*! State of the track at this node (smoothed)*/ 
  TFRState m_state_sm;
  
  //magic happens here
  ClassDef(TFRFitnode, 1)
};

/*! Array of tracks */
typedef TObjArray TFRFitnodes;

#endif // INCLUDE_TFRFITNODE_H
