#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ class TFRParticle+;
#pragma link C++ class TFRTrack+;
#pragma link C++ class TFREvent+;
#pragma link C++ class TFRMCHit+;
#pragma link C++ class TFRCluster+;
#pragma link C++ class TFRLayer+;
#pragma link C++ class TFRState+;
#pragma link C++ class TFRFitnode+;

#pragma link C++ defined_in "TFRParticle.h";
#pragma link C++ defined_in "TFRTrack.h";
#pragma link C++ defined_in "TFREvent.h";
#pragma link C++ defined_in "TFRMCHit.h";
#pragma link C++ defined_in "TFRCluster.h";
#pragma link C++ defined_in "TFRLayer.h";
#pragma link C++ defined_in "TFRState.h";
#pragma link C++ defined_in "TFRFitnode.h";
#endif /* __CINT__ */
