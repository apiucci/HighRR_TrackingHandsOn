/*!
 *  @author    Alessio Piucci
 *  @brief     Representation of a detector layer
 */

#include "TFRLayer.h"

ClassImp(TFRLayer)

//----------
// Constructor
//-----------
TFRLayer::TFRLayer(const unsigned int _layer_id, const TEveVectorD _center_position,
                   const double _phi, const double _theta, const double _psi,
                   const double _x_size, const double _y_size, const double _dxdy,
                   const std::string _sensor_type,
                   const double _hit_resolution, const double _hit_efficiency,
                   const double _noise_prob,
                   const double _x_segm, const double _y_segm,
		   const double _x_over_Xzero){
  
  //layer ID
  layer_id = _layer_id;
  
  //plane position
  center_position = _center_position;

  //set the Euler angles
  phi = _phi;
  theta = _theta;
  psi = _psi;
  
  //compute the rotation matrix in the cartesian coordinate system
  rot_matrix[0][0] = cos(psi)*cos(phi) -cos(theta)*sin(phi)*sin(psi);
  rot_matrix[1][0] = - sin(psi)*cos(phi) -cos(theta)*sin(phi)*cos(psi);
  rot_matrix[2][0] = sin(theta)*sin(phi);
  rot_matrix[0][1] = cos(psi)*sin(phi) + cos(theta)*cos(phi)*sin(psi);
  rot_matrix[1][1] = -sin(psi)*sin(phi) + cos(theta)*cos(phi)*cos(psi);
  rot_matrix[2][1] = -sin(theta)*cos(phi);
  rot_matrix[0][2] = sin(psi)*sin(theta);
  rot_matrix[1][2] = cos(psi)*sin(theta);
  rot_matrix[2][2] = cos(theta);

  //compute the versor orthogonal to the plane
  plane_ortversor.Set(this->rot_matrix[2][0],
		      this->rot_matrix[2][1],
		      this->rot_matrix[2][2]);

  //get the the layer dimensions
  x_size = _x_size;
  y_size = _y_size;
  
  //get the angular tilt on the xy plane and transform it into radians
  dxdy = _dxdy;


  /*
  //set the the layer boundaries in the detector reference system
  x_max = center_position[0] + (y_size/2)*cos(dxdy) + (x_size/2)*sin(dxdy);
  x_min = center_position[0] - (y_size/2)*cos(dxdy) - (x_size/2)*sin(dxdy);

  y_max = center_position[1] + (y_size/2)*sin(dxdy) + (x_size/2)*cos(dxdy);
  y_min = center_position[1] - (y_size/2)*sin(dxdy) - (x_size/2)*cos(dxdy);
  */
  
  //set the sensor details
  sensor_type = _sensor_type;

  //set the projection matrix
  if((sensor_type == "PIXEL")){
    double h[10] = {1,0,0,0,0,
                    0,1,0,0,0};
    proj_matrix.ResizeTo(2,5);
    proj_matrix = TMatrixD(2,5,h);
  }
  
  if((sensor_type == "STRIP")){
    double h[5] = {cos(dxdy),-sin(dxdy),0,0,0};
    proj_matrix.ResizeTo(1,5);
    proj_matrix = TMatrixD(1,5,h);
  }
  
  if((sensor_type != "PIXEL") && (sensor_type != "STRIP")){
    std::cout << "TFRLayer:: Error: sensor type not recognized." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  hit_resolution = _hit_resolution;
  hit_efficiency = _hit_efficiency;
  noise_prob = _noise_prob;
  
  x_segm = _x_segm;
  y_segm = _y_segm;

  x_over_Xzero = _x_over_Xzero;
  
}

//-----------
// Check if a position falls in the layer
//-----------
bool TFRLayer::InLayer(const TEveVectorD position){

  //it's convenient to move into the reference system of the layer,
  //where the sensors are not tilted
  double x_hit = (position[0] - center_position[0]);
  double y_hit = (position[1] - center_position[1]);

  x_hit = x_hit*cos(this->dxdy) + y_hit*sin(this->dxdy);
  y_hit	= -x_hit*sin(this->dxdy) + y_hit*cos(this->dxdy);
  
  //check if the intersection is within the layer boundaries
  if((fabs(x_hit) < (this->x_size/2.))
     && (fabs(y_hit) < (this->y_size/2.)))
    return true;
  else
    return false;

}

//----------
// Compute the channelID for a given sensor channel
//----------
std::string TFRLayer::GetChannelID(TFRLayer *layer, const double x, const double y){

  std::stringstream channelID_stream;

  //convention for the channel ID:
  // llXxxxxYyyyy
  //where ll is the layer_id,
  // xxxx = | x / segm_x | (approximated to smallest near integer) with X = 1 in case it's positive, = 0 negative 
  // yyyy = | y / segm_y | (approximated to smallest near integer) with Y = 1 in case it's positive, = 0 negative

  //BE CAREFUL: you should provide the x, y coordinates in the layer reference system!
  
  //fill the layer ID
  channelID_stream << std::setfill('0') << std::setw(2) << layer->GetID();
  
  //fill the x segmentation
  int x_segm = (int) std::floor(x / layer->GetXSegm());
  
  if(x_segm > 0)
    channelID_stream << "1";
  
  channelID_stream << std::setfill('0') << std::setw(4) << fabs(x_segm);
  
  //fill the y segmentation
  int y_segm = (int) std::floor(y / layer->GetYSegm());
  
  if(y_segm > 0)
    channelID_stream << "1";											

  channelID_stream << std::setfill('0') << std::setw(4) << fabs(y_segm);
  
  return channelID_stream.str();

}
