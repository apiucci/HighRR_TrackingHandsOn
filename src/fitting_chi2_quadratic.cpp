/*!
 *  @file      fitting_chi2_quadratic.cpp
 *  @author    Alessio Piucci
 *  @brief     Fitting of reconstructed tracks with minimization of chi2 of a straight line + a parabola
 */
 
//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <map>
#include <chrono>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

//ROOT libraries
#include "TROOT.h"
#include "TFile.h"

//custom libraries
#include "../include/cparser.h"
#include "../include/TFREvent.h"
#include "../include/TFRGeometry.h"
#include "../include/TFRTrack.h"
#include "../include/TFRChiSquaredFit.h"

using namespace std;

/*! print the instructions for this macro */
void print_instructions(){
  std::cout << std::endl;
  std::cout << "Mandatory options: " << std::endl;
  std::cout << "\t-i: input file name" << std::endl;
  std::cout << "\t-g: config file name containing the geometry" << std::endl;
  std::cout << "\t-o: output file name" << std::endl;
  std::cout << std::endl;
  exit(1);
}

/*! Chi2 fitting of a straight + a parabolic tracklets */
bool fitStraightAndParabola(TFRTrack *track,
			    TFRGeometry *detector_geo,
			    TFRChiSquaredFit *chi2_fit){

  //-------------------------------------------------//
  //  fitting of a straight + a parabolic tracklets  //
  //-------------------------------------------------//

  //in case of uniform magnetic field + a first sub-detector out of B + a second sub-detector in B:
  //I perform a linear fitting of the first tracklet and a parabolic fitting of the second one
  //and then I estimate the momentum from the parabolic tracklet
  TFRTrack *track_beforeB = new TFRTrack();
  TFRTrack *track_inB = new TFRTrack();

  //-----------------------------//
  //  compose the two tracklets  //
  //-----------------------------//
  
  //loop over the clusters, to form the two tracklets
  //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
  //but just this stupid way (that I even didn't used at my C course during my bachelor)
  TIter it_cluster((TFRClusters*) track->GetClusters());
  TFRCluster *curr_cluster;

  //loop over the clusters
  while((curr_cluster = (TFRCluster*) it_cluster.Next())){

    //before magnetic region?
    if(curr_cluster->GetZ() < detector_geo->GetBFieldStartZ())
      track_beforeB->AddCluster(curr_cluster);

    //in magnetic region?
    if((curr_cluster->GetZ() > detector_geo->GetBFieldStartZ())
       && (curr_cluster->GetZ() < detector_geo->GetBFieldEndZ()))
      track_inB->AddCluster(curr_cluster);
    
  }  //loop over the clusters

  //check that everything went fine
  if((track_beforeB->GetNClusters() <= 0)
     || (track_inB->GetNClusters() <= 0)){
    std::cout << "Error: the tracklets before and in B are not correctly set" << std::endl;
    return false;
  }
  
  //-------------------------------------------//
  //  now I can finally fit the two tracklets  //
  //-------------------------------------------//

  //fit the straight tracklet
  if(!chi2_fit->FitLinear1D_matrix(track_beforeB, true, detector_geo)){
    std::cout << "Error: problem with linear chi2 fitting of the tracklet before B." << std::endl;
    track->SetFitStatus(false);
    return false;
  }
  
  //fit the parabolic tracklet
  if(!chi2_fit->FitQuadratic1D_matrix(track_inB, true, detector_geo)){
    std::cout << "Error: problem with parabolic chi2 fitting of the tracklet in B." << std::endl;
    track->SetFitStatus(false);
    return false;
  }

  //I assign to the track the parameters of the tracklet which is most near to the vertex
  track->SetTrackParamsXZ(track_beforeB->GetTrackParamsXZ());
  
  //----------------//
  //  set the chi2  //
  //----------------//
  /*
  //check that the tracklets have a reasonable chi2
  if((track_beforeB->GetChi2NdoF() > 20.)
     || (track_inB->GetChi2NdoF() > 20.)){
    std::cout << "Warning: too high chi2 of the tracklets."
              << " chi2NdoF_beforeB = " << track_beforeB->GetChi2NdoF()
              << ", chi2NdoF_inB = " << track_inB->GetChi2NdoF() << std::endl;
    track->SetFitStatus(false);
    return false;
  }  //check the chi2 of the tracklets
  */
  //now combine into the total chi2 of the track
  track->SetChi2(track_beforeB->GetChi2() + track_inB->GetChi2());
  
  //two free parameters of the straight tracklet
  // + 3 free parameters of the parabolic tracklet = 5 free parameters
  track->SetNdoF(track_beforeB->GetNClusters() + track_inB->GetNClusters() - 5);
  track->SetChi2NdoF(track->GetChi2() / track->GetNdoF());

  //----------------------------------------------//
  //  estimate the momentum on the bending plane  //
  //----------------------------------------------//

  //get the radius R = -1./2c
  double R = -1. / (2 * track_inB->GetTrackParamsXZ()[2]);
  double R_err = 2. / pow(2 * track_inB->GetTrackParamsXZ()[2],2)*track_inB->GetErrorParamsXZ()[2];

  //set the charge
  if((R * detector_geo->GetBMag()) > 0.)
    track->SetCharge(1);
  else
    track->SetCharge(-1);
  
  //by definition R[m] = p[GeV] / ( c[m/s]*1e-9 *q[e] *B[T])
  double momentum_xz = R/100. * (track->GetCharge() * detector_geo->GetBMag() * 299792485*1e-9);
  double momentum_xz_err = R_err/100. * (track->GetCharge() * detector_geo->GetBMag() * 299792485*1e-9);
  
  //convert the momentum from GeV/c to MeV/c:
  momentum_xz *= 1000.;

  //------------------------------//
  //  linear fit on the xy plane  //
  //------------------------------//

  //fit the full track with a straight line on yz plane: there's no bending!
  if(!chi2_fit->FitLinear1D_matrix(track, false, detector_geo)){
    std::cout << "Error: problem with linear chi2 fitting of the tracklet before B on the yz plane." << std::endl;
    track->SetFitStatus(false);
    return false;
  }
  
  //-------------------------//
  //  set the full momentum  //
  //-------------------------//

  //In order to obtain the correct uncertainty on the momentum (not direction):
  //ignore the correlation between px,pz and ignore the uncertainty on SlopeX
  double px     = momentum_xz * sin(track_beforeB->GetSlopeX());
  double px_err = momentum_xz_err * sin(track_beforeB->GetSlopeX());
  double pz     = momentum_xz * cos(track_beforeB->GetSlopeX());
  double pz_err = momentum_xz_err * cos(track_beforeB->GetSlopeX());
  
  //the slope is ty = py/pz --> py = by * pz
  double py     = track->GetSlopeY() * pz;
  
  //Here the uncertainty should be included in order to get the correct coverage for the magnitude of the momentum..
  double py_err = track->GetSlopeY() * pz_err;
  
  //set the momentum
  track->SetMomentum(TVector3(px, py, pz));
  track->SetMomentumErr(TVector3(px_err, py_err, pz_err));

  //the track is succesfully fitted!
  track->SetFitStatus(true);
  
  return true;
}

/*! Fit the track (before B) with a 2D straight-chi2 fit and extrapolate to the vertex */
bool extrapolateVertex(TFRTrack *track,
		       TFRGeometry *detector_geo,
		       TFRChiSquaredFit *chi2_fit){

  //----------------------------------------------//
  //  loop over the clusters,                     //
  //  to form two tracklet before magnetic field  //
  //----------------------------------------------//
  TFRTrack *track_beforeB = new TFRTrack();
  
  //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
  //but just this stupid way (that I even didn't used at my C course during my bachelor)
  TIter it_cluster((TFRClusters*) track->GetClusters());
  TFRCluster *curr_cluster;
  
  //loop over the clusters
  while((curr_cluster = (TFRCluster*) it_cluster.Next())){
    
    //before magnetic region?
    if(curr_cluster->GetZ() < detector_geo->GetBFieldStartZ())
      track_beforeB->AddCluster(curr_cluster);
    
  }  //loop over the clusters

  
  //----------------------------//
  //  2D straight chi2 fitting  //
  //----------------------------//
  
  //fit the straight tracklet with the 2D chi2 fit
  if(!chi2_fit->FitLinear2D_matrix(track_beforeB, detector_geo)){
    std::cout << "Error: problem with linear 2D chi2 fitting of the tracklet before B." << std::endl;
    track->SetFitStatus(false);
    return false;
  }
  
  //check that the tracklet have a reasonable chi2
  if(track_beforeB->GetChi2NdoF() > 20.){
    std::cout << "Warning: too high chi2 of the tracklet."
      << " chi2NdoF_beforeB = " << track_beforeB->GetChi2NdoF()
      << std::endl;
    track->SetFitStatus(false);
    return false;
  }  //check the chi2 of the tracklets


  //-----------------------------------------//
  //  propagate the parameters               //
  //  from the center of the track to z = 0  //
  //-----------------------------------------//

  //for straight lines, I can define
  // p0 = (a)
  //      (b)
  //and propagating it of delta_z:
  // p1 = (a + b*delta_z)
  //      (     b       )
  //Then the covariance matrix is:
  // V1 = (Dt * V0-1 * D)-1
  //where:
  // D = dp1 / dp0 = (1 delta_z)
  //                 (0    1   )

  //in case of x and y views together, I would say that D become:
  // D = dp1 / dp0 = (1  delta_z   0     0   )
  //                 (0    1       0     0   )
  //                 (0    0       1  delta_z) 
  //                 (0    0       1     1   )

  /*
  //define delta_z: from the center of the track to the origin
  double delta_z;

  //loop over the clusters of the track to find the center of the track
  double z_min = 999999.;
  double z_max = -999999.;

  it_cluster = (TFRClusters*) track_beforeB->GetClusters();
  
  //loop over the clusters
  while((curr_cluster = (TFRCluster*) it_cluster.Next())){

    if(curr_cluster->GetZ() < z_min)
      z_min = curr_cluster->GetZ();

    if(curr_cluster->GetZ() > z_max)
      z_max = curr_cluster->GetZ();
    
  }  //loop over the clusters

  //only true if I want to extrapolate to the origin!
  delta_z = ((z_max - z_min)/2.) + z_min;
    
  //build the D matrix
  TMatrixD D(4, 4);
  
  D(0, 0) = 1.;
  D(0, 1) = delta_z;
  D(1, 0) = 0.;
  D(1, 1) = 1.;

  //let's copy the above sub-matrix
  D.SetSub(2, 2, D.GetSub(0, 1, 0, 1));
  
  //transpose the D matrix
  TMatrixD Dt(TMatrixD::kTransposed, D);

  //get the covariance matrix, not taking in account the c quadratic parameter
  TMatrixD V0(4, 4);

  V0.SetSub(0, 0, (track_beforeB->GetCovarianceMatrix()).GetSub(0, 1, 0, 1));
  V0.SetSub(2, 2, (track_beforeB->GetCovarianceMatrix()).GetSub(3, 4, 3, 4));
  
  //invert the covariance matrix: remeber to take the 2x2 sub-matrix of a-b parameters only!
  TMatrixD V0_inv(TMatrixD::kInverted, V0);
  
  //propagate the covariance matrix
  TMatrixD V1(TMatrixD::kInverted,
	      Dt * V0_inv * D);
  */
  
  //set the initial state of the track
  //x0 = ax, y0 = ay, tx = bx, ty = by, qop = does not matter
  TFRState *initial_state = new TFRState();

  //is supposed that you have already measured the momentum!
  double vect_temp[5] = {track_beforeB->GetTrackParamsXZ()[0],
			 track_beforeB->GetTrackParamsYZ()[0],
			 track_beforeB->GetTrackParamsXZ()[1],
			 track_beforeB->GetTrackParamsYZ()[1],
			 track->GetCharge() / track->GetMomentum().Mag()};
  
  initial_state->SetStateVect(vect_temp);

  //set the covariance matrix of the state
  TMatrixD cov_matrix(5, 5);

  /*
  cov_matrix(0, 0) = V1(0, 0);  //dax^2
  cov_matrix(2, 0) = V1(0, 1);  //dax*dbx
  cov_matrix(0, 2) = V1(1, 0);  //dax*dbx 
  cov_matrix(2, 2) = V1(1, 1);  //dbx^2 

  cov_matrix(1, 1) = V1(2, 2);  //day^2
  cov_matrix(3, 1) = V1(3, 2);  //day*dby
  cov_matrix(1, 3) = V1(2, 3);   //day*dby
  cov_matrix(3, 3) = V1(3, 3);  //dby^2
  */
  
  cov_matrix(0, 0) = track_beforeB->GetCovarianceMatrixXZ()(0, 0);  //dax^2
  cov_matrix(2, 0) = track_beforeB->GetCovarianceMatrixXZ()(1, 0);  //dax*dbx
  cov_matrix(0, 2) = track_beforeB->GetCovarianceMatrixXZ()(0, 1);  //dax*dbx
  cov_matrix(2, 2) = track_beforeB->GetCovarianceMatrixXZ()(1, 1);  //dbx^2
  
  cov_matrix(1, 1) = track_beforeB->GetCovarianceMatrixYZ()(0, 0);  //day^2
  cov_matrix(3, 1) = track_beforeB->GetCovarianceMatrixYZ()(1, 0);  //day*dby
  cov_matrix(1, 3) = track_beforeB->GetCovarianceMatrixYZ()(0, 1);  //day*dby
  cov_matrix(3, 3) = track_beforeB->GetCovarianceMatrixYZ()(1, 1);  //dby^2
  
  cov_matrix(4, 4) = pow(track->GetCharge()/pow(track->GetMomentumMod(),2)*track->GetMomentumErrMod(),2);
  //Negelct correlations of momentum and x,y,tx,ty
  
  initial_state->SetP(cov_matrix);

  track->SetInitialState(initial_state);
  
  //the track is succesfully fitted!
  track->SetFitStatus(true);
  
  /*
  track->SetTrackParamsXZ(track_beforeB->GetTrackParamsXZ());
  track->SetTrackParamsYZ(track_beforeB->GetTrackParamsYZ());
  track->SetCovarianceMatrix(track_beforeB->GetCovarianceMatrix());
  
  track->SetChi2(track_beforeB->GetChi2());
  track->SetChi2NdoF(track_beforeB->GetChi2NdoF());
  */
  
  return true;
  
};

int main(int argc, char **argv){

  std::cout << std::endl;
  std::cout << "---> chi2 quadratic fitting" << std::endl;
  
  //--------------------------------------------//
  //  retrieve the job options from the parser  //
  //--------------------------------------------//

  CParser cmdline(argc, argv);

  //check the parsed options
  if(argc == 1)
    print_instructions();
  else{
    if((cmdline.GetArg("-i") == "") || (cmdline.GetArg("-g") == "")
       || (cmdline.GetArg("-o") == ""))
      print_instructions();
  }

  //parse the options
  std::string inFile_name = cmdline.GetArg("-i");
  std::string configGeoFile_name = cmdline.GetArg("-g");
  std::string outFile_name = cmdline.GetArg("-o");

  //print the imported options
  std::cout << std::endl;
  std::cout << "inFile_name = " << inFile_name << std::endl;
  std::cout << "configGeoFile_name = " << configGeoFile_name << std::endl;
  std::cout << "outFile_name = " << outFile_name << std::endl;

  //open the output file
  TFile *outFile = new TFile(outFile_name.c_str(), "recreate");
  
  //----------------------------------------------------------//
  //  import the geometry and set the TFRChiSquaredFit class  //
  //----------------------------------------------------------//

  //create the detector geometry
  TFRGeometry *detector_geo = new TFRGeometry(configGeoFile_name);

  std::cout << "Geometry imported, TAG = " << detector_geo->GetTag() << std::endl;
  std::cout << "nLayers = " << detector_geo->GetNLayers() << std::endl;

  //set the TFRChiSquaredFit class
  TFRChiSquaredFit *chi2_fit = new TFRChiSquaredFit(detector_geo);
  
  //-------------------------------//
  //  import the simulated events  //
  //-------------------------------//
  
  //open the input file
  TFile *inFile = new TFile(inFile_name.c_str(), "read");

  if(inFile == NULL){
    std::cout << "Error: the input file does not exist" << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //get a clone of the input events
  TFREvents *event_list = (TFREvents*) inFile->Get("event_list");

  //check for the existence of the input events
  if(event_list == NULL){
    std::cout << "Error: the input event list does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //get a clone, I don't want to play with the original copy
  event_list = (TFREvents*) event_list->Clone();
  event_list->SetOwner(kTRUE);
  
  //some printouts
  std::cout << std::endl;
  std::cout << "number of events = " << event_list->GetEntries() << std::endl;
  std::cout << std::endl;

  //test
  //Hit *test_hit = new Hit(detector_geo->GetLayers()->at(0));
  //Particle *test_particle = new Particle(TEveVectorD(0., 0., 0.), TEveVectorD(0., 0., 0.), 0.);
  //test_particle->AddHit(test_hit);
  
  //-------------------------------//
  //  finally start with the fun!  //
  //-------------------------------//
  
  //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
  //but just this stupid way (that I even didn't used at my C course during my bachelor)
  TIter it_event(event_list);
  TFREvent *curr_event;

  //start to measure the timing
  std::chrono::high_resolution_clock::time_point t_start = std::chrono::high_resolution_clock::now();
  
  //loop over the events
  while((curr_event = (TFREvent*) it_event.Next())){
    
    //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
    //but just this stupid way (that I even didn't used at my C course during my bachelor)
    TIter it_track((TFRTracks*) curr_event->GetRecoTracks());
    TFRTrack *curr_track;
    
    //loop over the reconstructed tracks
    while((curr_track = (TFRTrack*) it_track.Next())){
      
      //fit a 1D straight tracklet + a 1D parabolic tracklet
      fitStraightAndParabola(curr_track, detector_geo, chi2_fit);

      //fit the track (before B) with a 2D straight-chi2 fit and extrapolate to the vertex
      extrapolateVertex(curr_track, detector_geo, chi2_fit);
      
      /*
      //get the current particle
      TFRParticle particle_temp = *((TFRParticle*) (((TFRParticles*) curr_track->GetParticles())->At(0)));

      //for debug, compare the generated and reconstructed momentum on the xz plane
      std::cout << "gen_momentum = (" << particle_temp.GetMomentum()[0]
		<< ", " << particle_temp.GetMomentum()[1]
		<< ", " << particle_temp.GetMomentum()[2]
      		<< "). reco_momentum = " << curr_track->GetMomentum()[0]
		<< ", " << curr_track->GetMomentum()[1]
		<< ", " << curr_track->GetMomentum()[2]
		<< "). ratio = (" << particle_temp.GetMomentum()[0] / curr_track->GetMomentum()[0]
		<< ", " << particle_temp.GetMomentum()[1] / curr_track->GetMomentum()[1]
		<< ", " << particle_temp.GetMomentum()[2] / curr_track->GetMomentum()[2]
		<< ")." << std::endl;
      */
      
    }  //loop over the reconstructed tracks
  }  //loop over the events

  //end to measure the timing
  std::chrono::high_resolution_clock::time_point t_end = std::chrono::high_resolution_clock::now();

  //print the timing
  std::cout << "total timing = "
            << ((std::chrono::duration_cast<std::chrono::microseconds>(t_end - t_start).count())/1000.)/event_list->GetEntries()
            << " ms/event." << std::endl;
  
  //write the output file
  outFile->cd();
  event_list->Write("event_list", TObject::kSingleKey);
  outFile->Write();
  
  return 0;
  
}
