/*!
 *  @file      reco_decay.cpp
 *  @author    Alessio Piucci
 *  @brief     Reconstruction of a two-body decay
 */
 
//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

//ROOT libraries
#include "TROOT.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include "TRandom3.h"

//custom libraries
#include "../include/cparser.h"
#include "../include/TFREvent.h"
#include "../include/TFRGeometry.h"
#include "../include/TFRTrack.h"

using namespace std;

/*! Particles masses [MeV/c^2], used to reconstruct the mother */
const double pi_mass = 139.57;
const double kaon_mass = 497.61;

/*! print the instructions for this macro */
void print_instructions(){
  std::cout << std::endl;
  std::cout << "Mandatory options: " << std::endl;
  std::cout << "\t-i: input file name" << std::endl;
  std::cout << "\t-g: config file name containing the geometry" << std::endl;
  std::cout << "\t-o: output file name" << std::endl;
  std::cout << "\t-k: use (1) or not (0) the Kalman reconstructed tracks" << std::endl;
  std::cout << std::endl;
  exit(1);
}

/*! Constrain the tracks to come from the same vertex */
TVector3 ConstrainVertex(TFRTracks* reco_tracks){
  
  //reference:
  //http://www.hephy.at/fileadmin/user_upload/Fachbereiche/ASE/Strandlie.pdf
  
  //I define h(v, p) the state of the track
  //for given values of the parameters v, p.
  //Without magnetic field, h is:
  // h = (x0 ) = (x - z*px/pz)
  //     (y0 )   (x - z*py/pz)
  //     (tx )   (px/pz      )
  //     (ty )   (py/pz      )
  //     (q/p)   (q/p        )

  //then I define
  // A = dh / dv | v', p'
  // where v' and p' are some approximated values of the vertex and momentum
  // (like the values extrapolated to the beam line)
  // A = (dx0/dx     dx0/dy     dx0/dz)    |
  //     (dy0/dx     dy0/dy     dy0/dz)    |
  //     (dtx/dx     dtx/dy     dtx/dz)    |
  //     (dty/dx     dty/dy     dty/dz)    |
  //     (d(qop)/dx  d(qop)/dy  d(qop)/dz) | v', p'
  //
  //   = (1   0   -px/pz) |
  //     (1   0   -py/pz) |
  //     (0   0      0  ) |
  //     (0   0      0  ) |
  //     (0   0      0  ) | v', p'
  
  //now I define
  // B = dh / dp | v', p'
  // having:
  // B = (dx0/dpx     dx0/dpy     dx0/dpz)    |
  //     (dy0/dpx     dy0/dpy     dy0/dpz)    |
  //     (dtx/dpx     dtx/dpy     dtx/dpz)    |
  //     (dty/dpx     dty/dpy     dty/dpz)    |
  //     (d(qop)/dpx  d(qop)/dpy  d(qop)/dpz) | v', p'
  //
  //   = (-z/pz       0         z*px/pz^2) |
  //     (0          -z/pz      z*py/pz^2) |
  //     (1/pz       0          -px/pz^2 ) |
  //     (0          1/pz       -py/pz^2 ) |
  //     (-q*px/p^3  -q*py/p^3  -q*pz/p^3) | v', p'

  //I define
  // G = V-1
  // where V is the covariance matrix of the track parameters

  //and also
  // W = (Bt * G * B)-1
  // G^B = G - G * B * W * Bt * G
  
  //defining the covariance matrix of the vertex estimation
  // C = [\sum_itrack (At_itrack * G^B_itrack * A_itrack)]-1
  // I can finally compute the vertex estimation with:
  // v = C * [\sum_itrack (At_itrack * G^B_itrack * (q"_itrack - c_itrack))]
  // where q" is the set of track parameters already measured in some previous fit and
  // c = h(v', p') - A*v' - B*p'

  //...e che la madonn j'accumpagn

  //define G = V-1
  std::vector<TMatrixD> G;

  //define track parameters q"
  std::vector<TVectorD> q_meas;

  //define the A matrix
  std::vector<TMatrixD> A;

  //define the transposed A matrix
  std::vector<TMatrixD> At;
  
  //define the B matrix
  std::vector<TMatrixD> B;

  //define the G^B matrix
  std::vector<TMatrixD> G_B;
  
  //define the c vectors
  std::vector<TVectorD> c;
  
  //set the approximated values of v' and p'
  double v_array[3] = {0., 0., 0.};
  double p_array[3] = {1000., 1000., 1000.};
  
  TVectorD v_(3, v_array);
  TVectorD p_(3, p_array);

  
  //--------------------------------//
  //  set the matrices that I need  //
  //--------------------------------//

  //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
  //but just this stupid way (that I even didn't used at my C course during my bachelor)
  TIter it_track(reco_tracks);
  TFRTrack *curr_track;
  
  //loop over the tracks, to set everything that I need
  while((curr_track = (TFRTrack*) it_track.Next())){
    
    curr_track->GetInitialState()->GetStateVect().Print();
    curr_track->GetInitialState()->GetP().Print();
    
    //set G, the inverted covariance matrix of the track parameters
    TMatrixD G_temp(TMatrixD::kInverted, curr_track->GetInitialState()->GetP());
    
    G.push_back(G_temp);

    //set q", the track parameters measured at the first state
    q_meas.push_back(curr_track->GetInitialState()->GetStateVect());

    //set the A and B matrices
    double A_array[15] = {1.,  0.,  -p_(0)/p_(2),
                          1.,  0.,  -p_(1)/p_(2),
                          0.,  0.,  0.,
			  0.,  0.,  0.,
			  0.,  0.,  0.};

    double B_array[15] = {-v_(2)/p_(2),  0.,            v_(2)*p_(0)/pow(p_(2), 2.),
			  0.,            -v_(2)/p_(2),  v_(2)*p_(1)/pow(p_(2), 2.),
			  1./p_(2),      0.,            -p_(0)/pow(p_(2), 2.),
			  0.,            1./p_(2),      -p_(1)/pow(p_(2), 2.),
			  (-curr_track->GetCharge()*p_(0))/pow(p_.Norm2Sqr(), 3.),
			  (-curr_track->GetCharge()*p_(1))/pow(p_.Norm2Sqr(), 3.),
			  (-curr_track->GetCharge()*p_(2))/pow(p_.Norm2Sqr(), 3.)};
    
    TMatrixD A_temp(5, 3, A_array);
    TMatrixD B_temp(5, 3, B_array);
     
    A.push_back(A_temp);
    B.push_back(B_temp);
    
    //set h(v', p')
    double h_array[5] = {v_(0),
			 v_(1),
			 p_(0)/p_(2),
			 p_(1)/p_(2),
			 curr_track->GetCharge()/p_.Norm2Sqr()};

    TVectorD h(5, h_array);

    h.Print();
    A_temp.Print();
    B_temp.Print();
    
    //set c = h(v', p') - A*v' - B*p'
    c.push_back(h - A_temp*v_ - B_temp*p_);
    
    //set the transposed B
    TMatrixD Bt(TMatrixD::kTransposed, B_temp);

    Bt.Print();
    G_temp.Print();
    B_temp.Print();

    (Bt*G_temp*B_temp).Print();
    std::cout << (Bt*G_temp*B_temp).Determinant() << std::endl;
    
    //set the W matrix
    TMatrixD W(TMatrixD::kInverted, Bt*G_temp*B_temp);

    //set the transposed A
    At.push_back(TMatrixD(TMatrixD::kTransposed, A_temp));
    
    //set the G^B matrix
    G_B.push_back(G_temp - G_temp*B_temp*W*Bt*G_temp);

  }  //loop over the tracks, to set everything that I need

  //------------------------------//
  //  estimate the common vertex  //
  //------------------------------//

  //first compute the covariance matrix
  TMatrixD C(3, 3);
  
  //loop over the matrices of the tracks
  for(int i_track = 0; i_track < reco_tracks->GetEntries(); ++i_track)
    C += TMatrixD(TMatrixD::kInverted,
		  At.at(i_track) * G_B.at(i_track) * A.at(i_track)); 
  
  //now compute the common vertex
  TVectorD v(3);

  //loop over the matrices of the tracks
  for(int i_track = 0; i_track < reco_tracks->GetEntries(); ++i_track)
    v += (At.at(i_track) * G_B.at(i_track) * (q_meas.at(i_track) - c.at(i_track)));

  v *= C;

  v.Print();
  
  return TVector3(v(0), v(1), v(2));
}

int main(int argc, char **argv){

  std::cout << std::endl;
  std::cout << "---> decay reconstruction" << std::endl;
  
  //--------------------------------------------//
  //  retrieve the job options from the parser  //
  //--------------------------------------------//

  CParser cmdline(argc, argv);

  //check the parsed options
  if(argc == 1)
    print_instructions();
  else{
    if((cmdline.GetArg("-i") == "") || (cmdline.GetArg("-g") == "")
       || (cmdline.GetArg("-o") == "") || (cmdline.GetArg("-k") == ""))
      print_instructions();
  }

  //parse the options
  std::string inFile_name = cmdline.GetArg("-i");
  std::string configGeoFile_name = cmdline.GetArg("-g");
  std::string outFile_name = cmdline.GetArg("-o");
  unsigned int Kalman = std::stoul(cmdline.GetArg("-k"));
  
  //print the imported options
  std::cout << std::endl;
  std::cout << "inFile_name = " << inFile_name << std::endl;
  std::cout << "configGeoFile_name = " << configGeoFile_name << std::endl;
  std::cout << "outFile_name = " << outFile_name << std::endl;

  //open the output file
  TFile *outFile = new TFile(outFile_name.c_str(), "recreate");
  
  //----------------------------------------------------------//
  //  import the geometry and set the TFRChiSquaredFit class  //
  //----------------------------------------------------------//

  //create the detector geometry
  TFRGeometry *detector_geo = new TFRGeometry(configGeoFile_name);

  std::cout << "Geometry imported, TAG = " << detector_geo->GetTag() << std::endl;
  std::cout << "nLayers = " << detector_geo->GetNLayers() << std::endl;
  
  //-------------------------------//
  //  import the simulated events  //
  //-------------------------------//
  
  //open the input file
  TFile *inFile = new TFile(inFile_name.c_str(), "read");

  if(inFile == NULL){
    std::cout << "Error: the input file does not exist" << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //get a clone of the input events
  TFREvents *event_list = (TFREvents*) inFile->Get("event_list");

  //check for the existence of the input events
  if(event_list == NULL){
    std::cout << "Error: the input event list does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //get a clone, I don't want to play with the original copy
  event_list = (TFREvents*) event_list->Clone();
  event_list->SetOwner(kTRUE);
  
  //some printouts
  std::cout << std::endl;
  std::cout << "number of events = " << event_list->GetEntries() << std::endl;
  std::cout << std::endl;

  //Lorentz vectors used to combine the particles
  TLorentzVector qvect_mother;
  TLorentzVector qvect_1;
  TLorentzVector qvect_2;

  //PID informations of the daughters
  double daughter_1_mass;
  double daughter_2_mass;
  
  //set the random generator for the PID assignments
  TRandom3 *PID_random = new TRandom3();
  PID_random->SetSeed(0);
  
  //-------------------------------//
  //  finally start with the fun!  //
  //-------------------------------//
  
  //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
  //but just this stupid way (that I even didn't used at my C course during my bachelor)
  TIter it_event(event_list);
  TFREvent *curr_event;
  
  //loop over the events
  while((curr_event = (TFREvent*) it_event.Next())){

    //-----------------------------------------------------//
    //  constrain the tracks to come form the same vertex  //
    //-----------------------------------------------------//
    
    //constrain the tracks to come from the same vertex
    //TVector3 vertex = ConstrainVertex((TFRTracks*) curr_event->GetRecoTracks());
    TVector3 vertex(0., 0., 0.);
    
    //-----------------------------------------------------//
    //  combine the two tracks to get the mother particle  //
    //-----------------------------------------------------//

    //important: I assume that this is a two-body decay
    //then I don't need to loop over the tracks, but just to access them through indeces

    //check that the reconstructed tracks are existing
    if(curr_event->GetRecoTracks() == NULL){
      std::cout << "Error: there are no reconstructed tracks in the event." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    TFRTrack *track_1 = (TFRTrack*) ((TFRTracks*) curr_event->GetRecoTracks())->At(0);
    TFRTrack *track_2 = (TFRTrack*) ((TFRTracks*) curr_event->GetRecoTracks())->At(1);

    if((track_1 == NULL) || (track_2 == NULL)){
      std::cout << "Error: the reconstructed tracks are not correctly set." << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //set a random PID assignment
    if(PID_random->Uniform(-1., 1.) > 0.){
      daughter_1_mass = pi_mass;
      daughter_2_mass = kaon_mass;
    }
    else{
      daughter_1_mass = kaon_mass;
      daughter_2_mass = pi_mass;
    }

    //momentum of the daughter particles
    TVector3 momentum_1;
    TVector3 momentum_2;

    //should I use chi2 or Kalman reconstructed tracks?
    if(Kalman == 0){
      //chi2 momentum
      momentum_1 = track_1->GetMomentum();
      momentum_2 = track_2->GetMomentum();
    }
    else{
      //Kalman momentum
      momentum_1[0] = track_1->GetKalmanFilteredStateAt0()->GetMomentum()[0];
      momentum_1[1] = track_1->GetKalmanFilteredStateAt0()->GetMomentum()[1];
      momentum_1[2] = track_1->GetKalmanFilteredStateAt0()->GetMomentum()[2];
      
      momentum_2[0] = track_2->GetKalmanFilteredStateAt0()->GetMomentum()[0];
      momentum_2[1] = track_2->GetKalmanFilteredStateAt0()->GetMomentum()[1];
      momentum_2[2] = track_2->GetKalmanFilteredStateAt0()->GetMomentum()[2];
    }

    //set the momentum
    qvect_1.SetXYZM(momentum_1[0],
		    momentum_1[1],
		    momentum_1[2],
		    daughter_1_mass);
    
    qvect_2.SetXYZM(momentum_2[0],
		    momentum_2[1],
		    momentum_2[2],
		    daughter_2_mass);
    
    //combine the two particles and set the mother
    qvect_mother = qvect_1 + qvect_2;
    
    TFRParticle *mother = new TFRParticle(vertex,
					  TVector3(qvect_mother.Px(),
						   qvect_mother.Py(),
						   qvect_mother.Pz()),
					  qvect_mother.M(),
					  track_1->GetCharge() + track_2->GetCharge());
    
    curr_event->AddRecoSignalParticle(mother);
    
  }  //loop over the events
  
  //write the output file
  outFile->cd();
  event_list->Write("event_list", TObject::kSingleKey);
  outFile->Write();
  
  return 0;
  
}
