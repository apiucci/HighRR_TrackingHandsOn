/*!
 *  @file      decay_sim.cpp
 *  @author    Alessio Piucci
 *  @brief     This macro simulates the decay of a particle
 *  @returns   A .root output file with the simulated decays
 */

//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

//ROOT libraries
#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TRandom3.h"
#include "TObjArray.h"

//custom libraries
#include "../include/cparser.h"
#include "../include/TFREvent.h"

using namespace std;

/*! print the instructions for this macro */
void print_instructions(){
  std::cout << std::endl;
  std::cout << "Mandatory options: " << std::endl;
  std::cout << "\t-e: number of events to simulate" << std::endl;
  std::cout << "\t-i: input file name" << std::endl;
  std::cout << "\t-o: output file name" << std::endl;
  std::cout << std::endl;
  exit(1);
}

int main(int argc, char **argv){

  std::cout << std::endl;
  std::cout << "---> event_sim" << std::endl;
  
  //--------------------------------------------//
  //  retrieve the job options from the parser  //
  //--------------------------------------------//

  CParser cmdline(argc, argv);

  //check the parsed options
  if(argc == 1)
    print_instructions();
  else{
    if((cmdline.GetArg("-e") == "") ||
       (cmdline.GetArg("-i") == "") || (cmdline.GetArg("-o") == ""))
      print_instructions();
  }
  
  //parse the options
  unsigned int num_events = std::stoi(cmdline.GetArg("-e"));
  std::string inFile_name = cmdline.GetArg("-i");
  std::string outFile_name = cmdline.GetArg("-o");
  
  //print the imported options
  std::cout << std::endl;
  std::cout << "num_events = " << num_events << std::endl;
  std::cout << "inFile_name = " << inFile_name << std::endl;
  std::cout << "outFile_name = " << outFile_name << std::endl;

  
  //---------------------------//
  //  set the input variables  //
  //---------------------------//

  //open the input file
  TFile *inFile = new TFile(inFile_name.c_str(), "read");

  if(inFile == NULL){
    std::cout << "Error: the input file does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //retrieve the input tree
  TTree *inTree = (TTree*) inFile->Get("MCDecayTree");
  
  if(inTree == NULL){
     std::cout << "Error: the input tree does not exist." << std::endl;
    exit(EXIT_FAILURE);
   }
   
  //mother variables
  double mother_vx, mother_vy, mother_vz;        //origin vertex
  double mother_decx, mother_decy, mother_decz;  //decay vertex
  double mother_px, mother_py, mother_pz;
  double mother_E;
  
  //daughter variables
  double daughter1_px, daughter1_py, daughter1_pz;
  double daughter1_E;
  
  double daughter2_px, daughter2_py, daughter2_pz;
  double daughter2_E;

  //set the input variables from the input tree
  inTree->SetBranchAddress("D0_TRUEORIGINVERTEX_X", &mother_vx);
  inTree->SetBranchAddress("D0_TRUEORIGINVERTEX_Y", &mother_vy);
  inTree->SetBranchAddress("D0_TRUEORIGINVERTEX_Z", &mother_vz);
  inTree->SetBranchAddress("D0_TRUEENDVERTEX_X", &mother_decx);
  inTree->SetBranchAddress("D0_TRUEENDVERTEX_Y", &mother_decy);
  inTree->SetBranchAddress("D0_TRUEENDVERTEX_Z", &mother_decz);
  inTree->SetBranchAddress("D0_TRUEP_X", &mother_px);
  inTree->SetBranchAddress("D0_TRUEP_Y", &mother_py);
  inTree->SetBranchAddress("D0_TRUEP_Z", &mother_pz);
  inTree->SetBranchAddress("D0_TRUEP_E", &mother_E);
  
  inTree->SetBranchAddress("lab6_TRUEP_X", &daughter1_px);
  inTree->SetBranchAddress("lab6_TRUEP_Y", &daughter1_py);
  inTree->SetBranchAddress("lab6_TRUEP_Z", &daughter1_pz);
  inTree->SetBranchAddress("lab6_TRUEP_E", &daughter1_E);
  
  inTree->SetBranchAddress("lab7_TRUEP_X", &daughter2_px);
  inTree->SetBranchAddress("lab7_TRUEP_Y", &daughter2_py);
  inTree->SetBranchAddress("lab7_TRUEP_Z", &daughter2_pz);
  inTree->SetBranchAddress("lab7_TRUEP_E", &daughter2_E);

  /////////////
  
  //open the output file
  TFile *outFile = new TFile(outFile_name.c_str(), "recreate");

  //set the random generator for the charges
  TRandom3 *charge_random = new TRandom3();
  charge_random->SetSeed(0);
  
  //-----------------------//
  //  simulate the events  //
  //-----------------------// 
  
  //events
  TFREvents *event_list = new TFREvents();
  event_list->SetOwner(kTRUE);
    
  //loop over the events to simulate
  for(unsigned int i_event = 0; i_event < num_events; ++i_event){
    
    //get the current entry
    inTree->GetEntry(i_event);
    
    //set a new event
    TFREvent *event = new TFREvent();
    event->SetID(i_event);
    
    //std::cout << "evtID = " << event->GetID() << std::endl;
    
    //---------------------------//
    //  set the mother particle  //
    //---------------------------//
    
    //set the mother Lorentz vector
    TLorentzVector qvect_temp;

    qvect_temp.SetPxPyPzE(mother_px, mother_py, mother_pz,
			  mother_E);
    
    //set the mother particle
    TFRParticle *particle_mother = new TFRParticle(TVector3(mother_vx,
							    mother_vy,
							    mother_vz),
						   TVector3(mother_px,
							    mother_py,
							    mother_pz),
						   qvect_temp.M(),
						   0);  //let's assume neutral mother
    
    //set the particle ID
    particle_mother->SetID(i_event*3);

    //set the signal flag
    particle_mother->SetIsSignal(true);

    //--------------------------//
    //  set the first daughter  //
    //--------------------------//

    //charge of the first daughter
    int charge;
    
    if(charge_random->Uniform(-1, 1) > 0.)
      charge = 1;
    else
      charge = -1;
      
    //set the Lorentz vector for the first daughter
    qvect_temp.SetPxPyPzE(daughter1_px, daughter1_py, daughter1_pz,
                          daughter1_E);
    
    //set the first daughter
    TFRParticle *particle_daughter1 = new TFRParticle(TVector3(mother_decx,
							       mother_decy,
							       mother_decz),
						      TVector3(daughter1_px,
							       daughter1_py,
							       daughter1_pz),
						      qvect_temp.M(),
						      charge);  //let's assume neutral mother, for now
    
    //set the particle ID
    particle_daughter1->SetID(i_event*3 + 1);

    //---------------------------//
    //  set the second daughter  //
    //---------------------------//
    
    //set the Lorentz vector for the second daughter
    qvect_temp.SetPxPyPzE(daughter2_px, daughter2_py, daughter2_pz,
                          daughter2_E);

    //set the second daughter
    TFRParticle *particle_daughter2 = new TFRParticle(TVector3(mother_decx,
                                                               mother_decy,
                                                               mother_decz),
                                                      TVector3(daughter2_px,
                                                               daughter2_py,
                                                               daughter2_pz),
                                                      qvect_temp.M(),
                                                      -charge);  //opposite charge wrt the first daughter
    
    //set the particle ID
    particle_daughter2->SetID(i_event*3 + 2);

    
    //-------------------------------------//
    //  finalisation of the current event  //
    //-------------------------------------//
    
    //add the particles to the event
    event->AddGenParticle(particle_mother);
    event->AddGenParticle(particle_daughter1);
    event->AddGenParticle(particle_daughter2);
    
    //add the current event to the list of events
    event_list->Add(event);
    
  }  //loop over the events to simulate

  
  //-----------------------//
  //  write to the output  //
  //-----------------------//

  //write the events to the output file
  outFile->cd();
  event_list->Write("event_list", TObject::kSingleKey);
  
  //write the output file
  outFile->Write();

  return 0;
}
