/*!
 *  @file     plot_tracks.cpp
 *  @author   Alessio Piucci
 *  @brief    This scripts plot the tracks in a 3D space: amazing!
 */

//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

//ROOT libraries
#include "TROOT.h"
#include "TFile.h"
#include "TEveTrack.h"
#include "TEveElement.h"
#include "TEveViewer.h"
#include "TEveBrowser.h"
#include "TEveManager.h"
#include "TSystem.h"
#include "TObjArray.h"

#include "TEveTrackPropagator.h"

using namespace std;

void plot_tracks(std::string inputFile_name="/home/alessio/Dropbox/hands-on_tracking/output/track_test.root",
		 std::string inputTrackList="track_list"){
  
  //initialize the graphic application
  TEveManager::Create();
  gSystem->ProcessEvents();

  //set the propagator
  TEveTrackPropagator *propagator = new TEveTrackPropagator();
  
  //setup the Runge Kutta stepper
  propagator->SetStepper(TEveTrackPropagator::kRungeKutta);

  //set a uniform magnetic field on y direction
  propagator->SetMagField(1., 0., 0.);
  
  //some other options
  propagator->SetFitDaughters(kFALSE);      //do not follow the daughters particles
  propagator->SetMaxZ(1000.);               //maximum z-coordinate to propagate, cm
  propagator->SetMaxR(100000000);           //maximum bending radius to propagate, cm
  propagator->SetDelta(pow(10., -6.));      //'error' on trajectory reconstruction

  TEveVectorD vertex(0., 0., 0.);
  TEveVectorD momentum(1., 1., 1.);
  
  //set the track into the RKRoot format:
  //I need it to easily set initial vertex and momentum values
  TEveRecTrackD *track = new TEveRecTrackD();

  track->fV.Set(vertex);     //set the track vertex [cm]
  track->fP.Set(momentum);   //set the track initial momentum [GeV/c?]
  
  //set the track into the final format that we need
  TEveTrack* prop_track = new TEveTrack(track, propagator);

  //set the charge
  prop_track->SetCharge(1.);
  
  //initialize the propagator
  prop_track->GetPropagator()->InitTrack(vertex, prop_track->GetCharge());

  prop_track->MakeTrack();

  //add the track to the event
  gEve->AddElement(prop_track);
  
  //process the event, and draw it
  gSystem->ProcessEvents();
  gEve->Redraw3D(kFALSE, kTRUE);
  
  return;
}
